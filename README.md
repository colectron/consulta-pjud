Consulta Poder Judicial
=======================

Carga la causa solicitada junto con la página principal de consultas del poder judicial (pero oculta), esto para poder crear la sesión requerida para hacer las consultas.

Disponible en <http://pjud.colectron.cl/consulta.php>
